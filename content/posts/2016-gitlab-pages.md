Title: Pelican on GitLab Pages!
Date: 2016-03-25
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

This site is hosted on GitLab Pages!
Isn't that awesome?

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
